#!/usr/bin/env python3

'''
Python distutils setup file for gym-platformer module.
Copyright (C) 2021 da_doomer
MIT License
'''

from setuptools import setup

setup(
    name='gym_platformer',
    version='0.1',
    install_requires=['gym', 'numpy'],
    description='Gym environment for simple platforming',
    packages=['gym_platformer'],
    author='da_doomer',
    url='https://gitlab.com/da_doomer/gym-platformer',
    license='MIT',
    platforms='Linux; Windows; OS X'
    )
