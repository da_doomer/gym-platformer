# gym-platformer

A very simple pure Python platformer environment with custom terrains.

![Random agent on gym-platformer.](README.gif)

*Random agent on gym-platformer.*

## Usage

```py
import gym_platformer
import gym

# Random terrain on every reset
env = gym.make("Platformer-v0")

# Custom terrain
terrain = [0, 1, 2, 0, 0]
env = gym.make("Platformer-v0", task=terrain)

observation = env.reset()
while True:
	env.render()
	action = env.action_space.sample()
	observation, reward, done, info = env.step(action)
	if done:
		observation = env.reset()
env.close()
```

## Description

For every episode the goal is to command the square to the rightmost section of
the level.

- **Actions:** two-dimensional acceleration commands in the range `[-1, 1]`.
	Thus, in practice the agent can only act while touching the ground, as a
	consequence only positive vertical accelerations have an effect on the
	dynamics.

- **Observations:** the current velocity and a local height-map of the
	terrain ahead.

- **Reward**: defined at each time-step as the velocity towards the goal.

- **Task:** the task defines the terrain. It is a sequence of platform heights.
