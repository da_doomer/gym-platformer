"""
Very simple platformer environment with custom terrains.
"""
# Native imports
from typing import Sequence
from typing import Tuple
from math import ceil

# External imports
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np


class Platformer(gym.Env):
    FPS = 50
    EPS = 0.001
    TOUCHING_WALL_EPS = 0.25
    TOUCHING_GROUND_EPS = 0.01
    SIMULATION_STEPS_PER_STEP = 5
    INTEGRATION_FACTOR = 1/(SIMULATION_STEPS_PER_STEP*FPS)
    RENDER_WIDTH = 600
    RENDER_HEIGHT = 400
    RENDER_POINT_SIZE_IN_PIXELS = 50
    RENDER_POINT_SIZE_IN_UNITS = TOUCHING_WALL_EPS*2
    SCREEN_CENTER_X = 300
    SCREEN_CENTER_Y = 200
    JUMP_TIMESTEPS_COOLDOWN = FPS//2
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': FPS
    }

    def __init__(
            self,
            task: Sequence[float] = None,
            max_input_acceleration: Tuple[float, float] = (100.0, 400.0),
            max_velocity: Tuple[float, float] = (1.0, 6.0),
            gravity: float = -1.0,
            max_timesteps: int = FPS*30,
            local_heightmap_len: int = 3,
            ):
        super().__init__()
        self.seed()
        self.viewer = None
        self.max_timesteps = max_timesteps
        self.sample_task_on_reset = task is None
        if task is None:
            task = self.sample_task()
        self.task = task
        self.timesteps = 0
        self.local_heightmap_len = local_heightmap_len
        self.max_input_acceleration = max_input_acceleration
        self.max_velocity = max_velocity
        self.gravity = gravity
        self.timesteps_until_jump_available = 0
        self.position = np.zeros((2,))
        self.velocity = np.zeros((2,))

        # Define the action and observation spaces
        action_high = np.array([1.0, 1.0])
        self.action_space = spaces.Box(
                -action_high,
                action_high,
                dtype=np.float32
            )
        observation_high = np.array([np.inf]*len(self.observation))
        self.observation_space = spaces.Box(
                -observation_high,
                observation_high,
                dtype=np.float32
            )

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def sample_task(self):
        task = [0]
        for _ in range(10):
            task.append(task[-1] + self.np_random.uniform(-1, 1))
        return task

    @property
    def x(self) -> float:
        return self.position[0]

    @x.setter
    def x(self, value):
        self.position[0] = value

    @property
    def y(self) -> float:
        return self.position[1]

    @y.setter
    def y(self, value):
        self.position[1] = value

    @property
    def vx(self) -> float:
        return self.velocity[0]

    @vx.setter
    def vx(self, value):
        self.velocity[0] = value

    @property
    def vy(self) -> float:
        return self.velocity[1]

    @vy.setter
    def vy(self, value):
        self.velocity[1] = value

    @property
    def state(self) -> Tuple[float, float, float, float]:
        return (self.x, self.y, self.vx, self.vy)

    @state.setter
    def state(self, value: Tuple[float, float, float, float]):
        self.x = value[0]
        self.y = value[1]
        self.vy = value[2]
        self.vz = value[3]

    @property
    def heightmap(self) -> Tuple[float, ...]:
        return tuple(self.task)

    @property
    def local_heightmap(self) -> Tuple[float, ...]:
        i = max(0, int(self.x))
        i = min(len(self.heightmap)-1, i)
        local_heightmap = list(self.heightmap[i:i+self.local_heightmap_len])
        while len(local_heightmap) < self.local_heightmap_len:
            local_heightmap.append(local_heightmap[-1])
        return tuple(local_heightmap)

    @property
    def relative_local_heightmap(self) -> Tuple[float, ...]:
        return tuple(
                height - self.position[1]
                for height in self.local_heightmap
            )

    @property
    def previous_platform(self) -> float:
        i = max(0, int(self.x))
        i = min(len(self.heightmap)-1, i)
        if i < 0:
            return self.heightmap[0]
        return self.heightmap[i-1]

    @property
    def current_platform(self) -> float:
        return self.local_heightmap[0]

    @property
    def touching_ground(self) -> bool:
        if int(self.x + self.TOUCHING_WALL_EPS) > self.x:
            # Check if touching the next platform
            if abs(self.y - self.local_heightmap[1]) < self.TOUCHING_GROUND_EPS:
                return True
        if int(self.x - self.TOUCHING_WALL_EPS) < int(self.x):
            # Check if touching the previous platform
            if abs(self.y - self.previous_platform) < self.TOUCHING_GROUND_EPS:
                return True
        # Check if touching
        return abs(self.relative_local_heightmap[0]) <= self.TOUCHING_GROUND_EPS

    @property
    def left_collision(self) -> bool:
        if int(self.x) <= 0:
            return False
        if self.previous_platform <= self.current_platform:
            return False
        if self.previous_platform <= self.y:
            return False
        if int(self.x) == self.x:
            distance_to_wall = self.x - int(self.x-self.EPS)
        else:
            distance_to_wall = self.x - int(self.x)
        if distance_to_wall > self.TOUCHING_WALL_EPS:
            return False
        return True

    @property
    def right_collision(self) -> bool:
        i = int(self.x)
        if i < 0:
            return False
        if self.relative_local_heightmap[1] <= self.relative_local_heightmap[0]:
            return False
        if self.relative_local_heightmap[1] <= self.TOUCHING_GROUND_EPS:
            return False
        if int(self.x) == self.x:
            distance_to_wall = ceil(self.x+self.EPS) - self.x
        else:
            distance_to_wall = ceil(self.x) - self.x
        if distance_to_wall > self.TOUCHING_WALL_EPS:
            return False
        return True

    @property
    def jump_available(self) -> bool:
        return self.timesteps_until_jump_available <= 0 and self.touching_ground

    @property
    def goal(self) -> Tuple[float, float]:
        return (len(self.task), self.task[-1])

    @property
    def relative_goal(self) -> Tuple[float, float]:
        xg, yg = self.goal
        return (xg-self.x, yg-self.y)

    @property
    def goal_direction(self) -> Tuple[float, float]:
        goal_norm = np.linalg.norm(self.relative_goal)
        if goal_norm <= 1:
            return self.relative_goal
        return np.array(self.relative_goal)/np.linalg.norm(self.relative_goal)

    @property
    def observation(self) -> Tuple[float, ...]:
        return tuple(self.velocity)\
                + tuple(self.relative_local_heightmap)

    @property
    def done(self) -> bool:
        if self.x < 0:
            return True
        if self.x > len(self.task):
            return True
        if self.timesteps >= self.max_timesteps:
            return True
        return False

    @property
    def info(self) -> dict:
        return dict()

    def _step_dynamics(self, action: Tuple[float, float]) -> None:
        gravity = np.array([0.0, self.gravity])
        action_ = np.array(action)

        # Restrict input action
        for i in range(len(action_)):
            if abs(action_[i]) > 1:
                action_[i] = np.sign(action_[i])
            action_[i] = action_[i] * self.max_input_acceleration[i]

        # For numerical reasons we don't allow jumping in successive timesteps
        self.timesteps_until_jump_available -= 1
        if not self.jump_available:
            action_[1] = 0.0
        elif action_[1] > 0.0:
            self.timesteps_until_jump_available = self.JUMP_TIMESTEPS_COOLDOWN

        # Apply action only if touching the ground
        if not self.touching_ground:
            action_ = np.zeros_like(action_)
        acceleration = action_ + gravity

        # Step dynamics
        # xddot = u
        # xdot = xdot + xddot * dt
        self.velocity = self.velocity + self.INTEGRATION_FACTOR*acceleration

        for i in range(len(self.velocity)):
            if abs(self.velocity[i]) > self.max_velocity[i]:
                self.velocity[i] = np.sign(self.velocity[i])\
                        * self.max_velocity[i]

        # Walls impose constraints on velocity
        if self.left_collision:
            self.velocity[0] = max(0.0, self.velocity[0])
        if self.right_collision:
            self.velocity[0] = min(0.0, self.velocity[0])

        # The floor imposes a constraint on velocity
        if self.touching_ground:
            self.velocity[1] = max(0.0, self.velocity[1])

        # Step dynamics
        # x = x + xdot * dt
        self.position = self.position + self.INTEGRATION_FACTOR*self.velocity

        # The walls impose constraints on position
        # We take advantage that we are dealing with integer-placed blocks
        if self.left_collision:
            self.position[0] = int(self.position[0]) + self.TOUCHING_WALL_EPS
        if self.right_collision:
            self.position[0] = ceil(self.position[0]) - self.TOUCHING_WALL_EPS

        # The floor imposes a constraint on position
        self.position[1] = max(self.position[1], self.local_heightmap[0])

    @property
    def reward(self) -> float:
        """The reward is defined as the velocity towards the goal:

        r = v dot g/|g|

        where v is the velocituy and g the goal relative to the current
        position."""
        v = self.velocity
        g = self.relative_goal/np.linalg.norm(self.relative_goal)
        r = float(v.dot(g))
        return max(0.0, r)

    def step(
            self,
            action: Tuple[float, float]
            ) -> Tuple[Tuple[float, ...], float, bool, dict]:
        """The two-dimensional action is interpreted as a unitary velocity
        vector that will be scaled according to the maximum speed. The returned
        observation is the current velocity and a local heightmap."""
        for _ in range(self.SIMULATION_STEPS_PER_STEP):
            self._step_dynamics(action)
        self.timesteps += 1
        return (self.observation, self.reward, self.done, self.info)

    def reset(self):
        self.position = np.zeros_like(self.position)
        self.velocity = np.zeros_like(self.velocity)
        if self.sample_task_on_reset:
            self.task = self.sample_task()
        self.close()
        self.timesteps = 0
        return self.observation

    def render(self, mode='human'):
        scale = self.RENDER_POINT_SIZE_IN_PIXELS/self.RENDER_POINT_SIZE_IN_UNITS
        if self.viewer is None:
            # Create a viewer
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(
                    self.RENDER_WIDTH,
                    self.RENDER_HEIGHT
                )

            # Draw the point
            d = self.RENDER_POINT_SIZE_IN_UNITS/2
            l = self.x-d
            r = self.x+d
            t = self.y+2*d
            b = self.y
            l = l*scale
            r = r*scale
            t = t*scale
            b = b*scale
            point = rendering.FilledPolygon([
                    (l, b),
                    (l, t),
                    (r, t),
                    (r, b)
                ])
            point.set_color(0.8, 0.8, 0.8)

            # Draw the terrain as a polygon
            polygons = list()
            for i, height in enumerate(self.heightmap):
                l = i
                r = i+1
                b = min(self.heightmap)-1
                t = height
                l = l*scale
                r = r*scale
                t = t*scale
                b = b*scale
                polygon = rendering.FilledPolygon([
                    (l, b),
                    (l, t),
                    (r, t),
                    (r, b)
                ])
                polygons.append(polygon)

            # Transform everything so that the point is in the center
            point_positioner = rendering.Transform()
            point_positioner.set_translation(
                    self.SCREEN_CENTER_X,
                    self.SCREEN_CENTER_Y,
                )
            self.terrain_positioner = rendering.Transform()
            self.terrain_positioner.set_translation(
                    -self.x*scale+self.SCREEN_CENTER_X,
                    -self.y*scale+self.SCREEN_CENTER_Y,
                )
            point.add_attr(point_positioner)
            for polygon in polygons:
                polygon.add_attr(self.terrain_positioner)

            # Add the terrain and the point to the viewer
            self.viewer.add_geom(point)
            for polygon in polygons:
                self.viewer.add_geom(polygon)

        # Translate the terrain
        self.terrain_positioner.set_translation(
                -self.x*scale+self.SCREEN_CENTER_X,
                -self.y*scale+self.SCREEN_CENTER_Y,
            )

        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def close(self):
        if self.viewer is not None:
            self.viewer.close()
            self.viewer = None


if __name__ == "__main__":
    env = Platformer()
    observation = env.reset()
    while True:
        env.render()
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        if done:
            observation = env.reset()
    env.close()
