import gym
from .gym_platformer import Platformer

gym.envs.register(
    id="Platformer-v0",
    entry_point="gym_platformer:Platformer",
)
